<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/post/{post}', 'PostController@show')->name('post');

Route::middleware('auth')->group(function() {
    Route::get('/admin', 'AdminController@index')->name('admin.index');

    Route::get('/admin/posts', 'PostController@index')->name('post.index');
    Route::get('/admin/posts/create', 'PostController@create')->name('post.create');
    Route::get('/admin/posts/edit/{post}', 'PostController@edit')->middleware('can:view,post')->name('post.edit');
    Route::post('/admin/posts', 'PostController@store')->name('post.store');
    Route::patch('/admin/posts/{post}/update', 'PostController@update')->name('post.update');
    Route::delete('/admin/posts/{post}', 'PostController@destroy')->name('post.destroy');

    Route::get('/admin/users/{user}/profile', 'UserController@show')->name('user.profile.show');
    Route::patch('/admin/users/{user}/update', 'UserController@update')->name('user.profile.update');
});
