<x-admin-master>
    @section('content')
        <h1>{{$user->name}}</h1>
        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('user.profile.update', $user)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    @if(!empty($user->image))
                        <div class="mb-4">
                            <img src="{{$user->image}}" class="rounded-circle w-25" alt="">
                        </div>
                    @endif

                    <div class="form-group">
                        <input type="file" name="image" id="" class="{{$errors->has('image') ? 'is-invalid' : ''}}">
                        @error('name')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="name">Namn</label>
                        <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" id="name" value="{{$user->name}}" required />
                        @error('name')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">E-post</label>
                        <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="email" value="{{$user->email}}" required />
                        @error('email')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password">Lösenord</label>
                        <input type="password" name="password {{$errors->has('password') ? 'is-invalid' : ''}}" class="form-control" id="password" />
                        @error('password')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="confirm_password">Bekräfta lösenord</label>
                        <input type="password" name="password_confirmation {{$errors->has('password_confirmation') ? 'is-invalid' : ''}}" class="form-control" id="password_confirmation" />
                        @error('password_confirmation')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                    <input type="submit" name="save_profile" class="btn btn-primary" value="Spara" />
                </form>
            </div>
        </div>
    @endsection
</x-admin-master>
