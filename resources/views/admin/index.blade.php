<x-admin-master>

    @section('content')
        @if(auth()->user()->hasRole('Admin'))
            <h1 class="h3 mb-4 text-gray-800">Admin dash!</h1>
        @endif
    @endsection

</x-admin-master>
