<x-admin-master>
    @section('content')
        <h1>Edit post</h1>
        <form action="{{route('post.update', $post->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" value="{{$post->title}}" id="title" />
            </div>

            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" class="form-control" id="content">{{$post->content}}</textarea>
            </div>

            <div class="form-group">
                <img src="{{$post->image}}" alt="" /><br>
                <label for="image">Image</label>
                <input type="file" name="image" class="form-control-file" id="image" />
            </div>

            <input type="submit" name="save_post" class="btn btn-primary" value="Submit" />
        </form>
    @endsection
</x-admin-master>
