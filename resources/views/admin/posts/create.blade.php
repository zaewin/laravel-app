<x-admin-master>
    @section('content')
        <h1>Create post</h1>
        <form action="{{route('post.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" id="title" />
            </div>

            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" class="form-control" id="content"></textarea>
            </div>

            <div class="form-group">
                <label for="image">Image</label>
                <input type="file" name="image" class="form-control-file" id="image" />
            </div>

            <input type="submit" name="save_post" class="btn btn-primary" value="Submit" />
        </form>
    @endsection
</x-admin-master>
