<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ (request()->is('admin/posts*')) ? 'active' : '' }}">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePosts" aria-expanded="true" aria-controls="collapsePosts">
        <i class="fas fa-fw fa-edit"></i>
        <span>Posts</span>
    </a>
    <div id="collapsePosts" class="collapse {{ (request()->is('admin/posts*')) ? 'show' : '' }}" aria-labelledby="headingPosts" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{ (request()->is('admin/posts')) ? 'active' : '' }}" href="{{ route('post.index') }}">List</a>
            <a class="collapse-item {{ (request()->is('admin/posts/create')) ? 'active' : '' }}" href="{{ route('post.create') }}">Create</a>
        </div>
    </div>
</li>
