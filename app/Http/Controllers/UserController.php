<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function show(User $user) {
        return view('admin.users.profile', compact('user'));
    }

    public function update(User $user) {
        $inputs = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'image' => ['image'],
            'password' => 'sometimes|required|string|min:8',
            'password_confirmation' => 'sometimes|required_with:password|same:password', // TODO: Undersök varför dessa inte fungerar
        ]);

        if(request('image')) {
            $inputs['image'] = request('image')->store('images'); // OBS! Behöver en symlink till /storage
        }

        $user->update($inputs);
        Session::flash('success-message', 'Profile updated');
        return redirect()->route('user.profile.show', $user);
    }
}
