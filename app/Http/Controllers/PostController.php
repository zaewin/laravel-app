<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function index() {
        $posts = Post::paginate(5);
        return view('admin.posts.index', compact('posts'));
    }

    public function show(Post $post) {
        return view('blog-post', compact('post'));
    }

    public function create() {
        return view('admin.posts.create');
    }

    public function edit(Post $post) {
        return view('admin.posts.edit', compact('post'));
    }

    public function store() {
        $inputs = request()->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'image' => 'image'
        ]);

        if(request('image')) {
            $inputs['image'] = request('image')->store('images');
        }

        auth()->user()->posts()->create($inputs);
        Session::flash('success-message', 'Post created!');
        return redirect()->route('post.index');
    }

    public function update(Post $post) {
        $inputs = request()->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'image' => 'image'
        ]);

        $post->title = request('title');
        $post->content = request('content');

        if(request('image')) {
            $post->image = request('image')->store('images');
        }

        $this->authorize('update', $post); // Check if user is allowed to update (App\Policies\PostPolicy)
        $post->save();
        Session::flash('success-message', 'Post updated!');
        return back();
    }

    public function destroy(Post $post) {
        $post->delete();
        Session::flash('success-message', 'Post was deleted');
        return back();
    }
}
