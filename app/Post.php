<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = []; // Allow all fillable (instead of $fillable[])
    public function user() {
        return $this->belongsTo(User::class);
    }

    // Image Mutator
    /*public function setImageAttribute($value) {
        $this->attributes['image'] = $value;
    }*/

    // Image Accessor
    public function getImageAttribute($value) {
        // Check if external image
        if (strpos($value, 'https://') !== FALSE || strpos($value, 'http://') !== FALSE) {
            return $value;
        }

        return asset('storage/' . $value);
    }
}
